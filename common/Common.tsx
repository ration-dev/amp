import styled from 'styled-components';

export const Spacer = styled.div<{ height?: number; width?: number }>`
  height: ${({ height }) => `${height ?? 0.1}rem`};
  width: ${({ width }) => `${width ?? 0.1}rem`};
  flex-shrink: 0;
  ${({ height, width }) => (!height && !width ? 'flex: 1;' : '')}
`;

export const Column = styled.div`
  display: flex;
  flex-direction: column;
  flex-shrink: 0;
`;

export const Row = styled.div`
  display: flex;
  flex-direction: row;
  flex-shrink: 0;
`;

export const ChipContainer = styled.div<{ borderColor?: string; backgroundColor?: string }>`
  height: 4.8rem;
  padding: 0 3.2rem;
  display: flex;
  align-items: center;
  justify-content: center;
  border: 0.1rem solid ${({ borderColor }) => borderColor ?? 'black'};
  border-radius: 2.4rem;
  margin: 0.8rem;
  background-color: ${({ backgroundColor }) => backgroundColor ?? 'transparent'};
  cursor: pointer;
`;

export const ChipText = styled.span<{ color?: string }>`
  font-size: 2.4rem;
  font-weight: 500;
  text-transform: uppercase;
  color: ${({ color }) => color ?? 'black'};
`;
