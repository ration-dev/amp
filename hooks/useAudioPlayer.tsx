import React, { useLayoutEffect } from 'react';
import { useState, useEffect, createContext } from 'react';
import { useCurrentEpisode } from './useCurrentEpisode';

const AudioPlayerContext = createContext<{
  currentTime: number;
  duration: number;
  isPlaying: boolean;
  setIsPlaying: (isPlaying: boolean) => void;
  setClickedTime: (currentTime: number) => void;
}>({ currentTime: 0, duration: 0, isPlaying: false, setIsPlaying: () => {}, setClickedTime: () => {} });

export const AudioPlayerContextProvider: React.FC = ({ children }) => {
  const [duration, setDuration] = useState<number>(0);
  const [currentTime, setCurrentTime] = useState<number>(0);
  const [isPlaying, setIsPlaying] = useState(false);
  const [clickedTime, setClickedTime] = useState<number | null>();
  const { currentEpisode } = useCurrentEpisode();

  useLayoutEffect(() => {
    const audio = document.getElementById('audio') as HTMLAudioElement;

    audio.load();

    // state setters wrappers
    const setAudioData = () => {
      setDuration(audio.duration);
      setCurrentTime(audio.currentTime);
    };

    const setAudioTime = () => setCurrentTime(audio.currentTime);

    // DOM listeners: update React state on DOM events
    audio.addEventListener('loadeddata', setAudioData);

    audio.addEventListener('timeupdate', setAudioTime);

    // React state listeners: update DOM on React state changes
    isPlaying ? audio.play() : audio.pause();

    // if (clickedTime && clickedTime !== currentTime) {
    //   audio.currentTime = clickedTime;
    //   setClickedTime(null);
    // }

    // effect cleanup
    return () => {
      audio.removeEventListener('loadeddata', setAudioData);
      audio.removeEventListener('timeupdate', setAudioTime);
    };
  }, [currentEpisode?.id]);

  useLayoutEffect(() => {
    const audio = document.getElementById('audio') as HTMLAudioElement;
    isPlaying ? audio.play() : audio.pause();
  }, [isPlaying]);

  useEffect(() => {
    const audio = document.getElementById('audio') as HTMLAudioElement;
    if (!clickedTime) return;
    audio.currentTime = clickedTime;
  }, [clickedTime]);

  return (
    <AudioPlayerContext.Provider value={{ currentTime, duration, isPlaying, setIsPlaying, setClickedTime }}>
      {children}
    </AudioPlayerContext.Provider>
  );
};

export function useAudioPlayer() {
  const context = React.useContext(AudioPlayerContext);
  if (context === undefined) {
    throw new Error('useAudioPlayer must be used within a AudioPlayerContextProvider');
  }
  return context;
}
