import * as React from 'react';
import { createContext, useState } from 'react';
import { Episode } from '../types/episode';

const CurrentEpisodeContext = createContext<{
  currentEpisode: Episode | null;
  setCurrentEpisode: (episode: Episode | null) => void;
}>({ currentEpisode: null, setCurrentEpisode: () => {} });

export const CurrentEpisodeProvider: React.FC = ({ children }) => {
  const [currentEpisode, setCurrentEpisode] = useState<Episode | null>(null);

  return (
    <CurrentEpisodeContext.Provider value={{ currentEpisode, setCurrentEpisode }}>
      {children}
    </CurrentEpisodeContext.Provider>
  );
};

export function useCurrentEpisode() {
  const context = React.useContext(CurrentEpisodeContext);
  if (context === undefined) {
    throw new Error('useCurrentEpisode must be used within a CurrentEpisodeProvider');
  }
  return context;
}
