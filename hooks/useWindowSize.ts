import { useEffect, useState } from 'react';

type WindowSize = {
  width: number;
  height: number;
};

export function useWindowSize() {
  const [size, setSize] = useState<WindowSize>({ width: 0, height: 0 });

  useEffect(() => {
    function onWindowSizeChanged() {
      setSize({ width: window.innerWidth, height: window.innerHeight });
    }

    onWindowSizeChanged();

    window.addEventListener('resize', onWindowSizeChanged);
    return () => window.removeEventListener('resize', onWindowSizeChanged);
  }, []);

  return size;
}
