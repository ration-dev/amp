const Icons = {
  Logo : '/icons/logo.svg',
  LogoArchive : '/icons/logoArchive.svg',
  Pause : '/icons/pause.svg',
  Play : '/icons/play.svg',
  Menu : '/icons/menu.svg',
  Plays : '/icons/plays.svg',
  ArrowDown : '/icons/arrowDown.svg',
  ArrowUp : '/icons/arrowUp.svg',
}

export default Icons;
