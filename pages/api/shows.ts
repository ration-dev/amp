require('dotenv').config();

import { GoogleSpreadsheet } from 'google-spreadsheet';
import { Episode } from '../../types/episode';

const { SHEET_ID, GOOGLE_SERVICE_ACCOUNT_EMAIL, GOOGLE_PRIVATE_KEY } = process.env;

export const get_data_from_spreadsheet = async (): Promise<Episode[]> => {
  const doc = new GoogleSpreadsheet(SHEET_ID);

  await doc.useServiceAccountAuth({
    client_email: GOOGLE_SERVICE_ACCOUNT_EMAIL,
    private_key: GOOGLE_PRIVATE_KEY,
  });

  await doc.loadInfo();

  const sheet = doc.sheetsByIndex[0];
  const rows = await sheet.getRows();

  const data = rows.map((row) => {
    return {
      id: row.rowIndex.toString(),
      title: row['Show Name'],
      description: row['Summary'],
      createdAt: row['Date'],
      url: row['Link'],
      mediaType: row['Type of Data']
    };
  });

  return data.sort((a,b) => new Date(b.createdAt).getTime() - new Date(a.createdAt).getTime());
};

export default async (req, res) => {
  console.debug('Fetching data from google...');

  console.time('fetch');
  const data = await get_data_from_spreadsheet();
  console.timeEnd('fetch');

  res.json(data);
};
