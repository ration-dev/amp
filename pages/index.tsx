import { GetStaticProps } from 'next';
import Head from 'next/head';
import React from 'react';
import styled from 'styled-components';
import { Column, Spacer } from '../common/Common';
import { AudioPlayerContextProvider } from '../hooks/useAudioPlayer';
import { CurrentEpisodeProvider } from '../hooks/useCurrentEpisode';
import Icons from '../styles/icons';
import { Episode } from '../types/episode';
import { AudioPlayer } from '../views/AudioPlayer';
import { EpisodeItem } from '../views/EpisodeItem';
import { FilterView } from '../views/FilterView';
import { HeaderView } from '../views/HeaderView';
import { get_data_from_spreadsheet } from './api/shows';

type Props = {
  episodes: Episode[];
};

export const getStaticProps: GetStaticProps<Props> = async () => {
  const episodes = await get_data_from_spreadsheet();
  console.log(episodes);
  return {
    props: {
      episodes,
    },
  };
};

function App({ episodes }) {
  return (
    <CurrentEpisodeProvider>
      <AudioPlayerContextProvider>
        <Column style={{ alignItems: 'center' }}>
          <Head>
            <link rel="preload" href="/fonts/TWKEverett-Regular-web.woff" as="font" crossOrigin="" />
            <link rel="preload" href="/fonts/TWKEverett-Medium-web.woff" as="font" crossOrigin="" />
          </Head>

          <HeaderView />
          <Body>
            <Spacer height={12.2} />
            <Spacer height={2.2} />
            <LogoArchive src={Icons.LogoArchive} />
            <Spacer height={3.2} />
            {/* <FilterView /> */}
            <Spacer height={3.2} />
            {episodes.map((episode: Episode) => (
              <EpisodeItem key={episode.id} episode={episode} />
            ))}
            <Spacer height={15.0} />
          </Body>
          <AudioPlayer />
        </Column>
      </AudioPlayerContextProvider>
    </CurrentEpisodeProvider>
  );
}

const Body = styled.div`
  display: flex;
  flex-direction: column;
  max-width: 156rem;
  margin: 0 3.2rem;
`;

const LogoArchiveWrapper = styled.div`
  flex: 1;
  max-width: 124.2rem;
  max-height: 11.6rem;
`;

const LogoArchive = styled.img`
  /* max-width: 124.2rem;
  max-height: 11.6rem; */
  width: 100%;
  height: 100%;
`;

export default App;
