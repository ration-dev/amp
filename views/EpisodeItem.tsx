import React from 'react';
import styled from 'styled-components';
import Icons from '../styles/icons';
import { Column, Row, Spacer } from '../common/Common';
import { useAudioPlayer } from '../hooks/useAudioPlayer';
import { useCurrentEpisode } from '../hooks/useCurrentEpisode';
import { Episode } from '../types/episode';
import moment from 'moment';
import { useWindowSize } from '../hooks/useWindowSize';

type Props = {
  episode: Episode;
};

export const EpisodeItem: React.FC<Props> = ({ episode }) => {
  const { currentEpisode, setCurrentEpisode } = useCurrentEpisode();
  const { isPlaying, setIsPlaying } = useAudioPlayer();
  const windowSize = useWindowSize();

  const isActive = currentEpisode?.id == episode.id;

  const formattedDate = moment(episode.createdAt, 'MM/DD/YYYY').format('MM.DD.YYYY');

  function buildMobileLayout() {
    return (
      <Column style={{ flex: 1 }}>
        <Row style={{ alignItems: 'center' }}>
          <Date>{formattedDate}</Date>
          <Spacer width={4.8} />
          <Spacer />
          <Column>
            <PlayIcon src={isActive && isPlaying ? Icons.Pause : Icons.Play} />
          </Column>
          <Spacer width={2} />
        </Row>
        <Spacer height={4} />
        <Column style={{ maxWidth: '500px', flex: 1 }}>
          <Title>{episode.title}</Title>
          <Spacer height={3.2} />
          <Description>{episode.description}</Description>
          <Spacer height={3.2} />
          {/* <Row>
            <PlaysIcon src={Icons.Plays} />
            <Spacer width={2.0} />
            <PlaysText>1534</PlaysText>
          </Row> */}
        </Column>
      </Column>
    );
  }

  function buildDesktopLayout() {
    return (
      <>
        <Date>{formattedDate}</Date>
        <Spacer width={4.8} />
        <Column style={{ flex: 1 }}>
          <Column style={{ maxWidth: '500px' }}>
            <Title>{episode.title}</Title>
            <Spacer height={3.2} />
            <Description>{episode.description}</Description>
            <Spacer height={3.2} />
            {/* <Row>
              <PlaysIcon src={Icons.Plays} />
              <Spacer width={2.0} />
              <PlaysText>1534</PlaysText>
            </Row> */}
          </Column>
        </Column>
        <Column>
          <PlayIcon src={isActive && isPlaying ? Icons.Pause : Icons.Play} />
        </Column>
        <Spacer width={2} />
      </>
    );
  }

  return (
    <Container
      isActive={isActive}
      onClick={() => {
        if (isActive) {
          setIsPlaying(!isPlaying);
          return;
        }
        setCurrentEpisode(episode);
        setIsPlaying(true);
      }}
    >
      {windowSize.width > 600 ? buildDesktopLayout() : buildMobileLayout()}
    </Container>
  );
};

const Container = styled(Row)<{ isActive: boolean }>`
  border: 0.1rem solid ${({ isActive }) => (isActive ? 'transparent' : '#f8f2f2')};
  box-sizing: border-box;
  border-radius: 3rem;
  padding: 3rem;
  margin: 1.6rem 0;
  ${({ isActive }) => isActive && 'box-shadow: 0px 0px 2.4rem #44b7ca;'}
  transition: all 300ms;
`;

const PlayIcon = styled.img`
  width: 7.4rem;
  height: 7.4rem;
`;

const Date = styled.span`
  font-size: 2.8rem;
  font-weight: 500;
  color: #f8f2f2;
  text-transform: uppercase;
  white-space: pre-line;
`;

const Title = styled.span`
  font-size: 4.8rem;
  line-height: 5rem;
  font-weight: 500;
  color: #f8f2f2;
  text-transform: uppercase;
  white-space: pre-line;
`;

const Description = styled.span`
  font-size: 2rem;
  line-height: 2.4rem;
  color: #f8f2f2;
  white-space: pre-line;
`;

const PlaysIcon = styled.img`
  width: 4.4rem;
  height: 2.8rem;
`;

const PlaysText = styled.span`
  font-size: 2rem;
  line-height: 2.4rem;
  color: #f8f2f2;
  white-space: pre-line;
`;
