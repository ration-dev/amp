import React from 'react';
import moment from 'moment';
import momentDurationFormatSetup from 'moment-duration-format';
momentDurationFormatSetup(moment as any);

import styled from 'styled-components';
import { Column, Row, Spacer } from '../common/Common';
import { useCurrentEpisode } from '../hooks/useCurrentEpisode';
import { useAudioPlayer } from '../hooks/useAudioPlayer';

export const AudioPlayer: React.FC = () => {
  const { currentTime, duration, setClickedTime } = useAudioPlayer();
  const { currentEpisode } = useCurrentEpisode();

  const curPercentage = duration == 0 ? 0 : (currentTime / duration) * 100;

  function formatDuration(duration: number) {
    return moment.duration({ seconds: duration }).format('hh:mm:ss', { trim: false });
  }

  function calcClickedTime(e: React.MouseEvent | MouseEvent | TouchEvent) {
    const clickPositionInPage = e instanceof TouchEvent ? e.targetTouches[0].pageX : e.pageX;

    const bar = document.querySelector('.bar__progress') as HTMLDivElement;
    const barStart = bar.getBoundingClientRect().left + window.scrollX;
    const barWidth = bar.offsetWidth;
    const clickPositionInBar = clickPositionInPage - barStart;
    const timePerPixel = duration / barWidth;
    return timePerPixel * clickPositionInBar;
  }

  function handleTimeDrag(e: React.MouseEvent | MouseEvent | TouchEvent) {
    setClickedTime(calcClickedTime(e));

    const updateTimeOnMove = (eMove: React.MouseEvent | MouseEvent | TouchEvent) => {
      setClickedTime(calcClickedTime(eMove));
    };

    document.addEventListener('mousemove', updateTimeOnMove);

    document.addEventListener('mouseup', () => {
      document.removeEventListener('mousemove', updateTimeOnMove);
    });

    document.addEventListener('touchmove', updateTimeOnMove);

    document.addEventListener('touchend', () => {
      document.removeEventListener('touchmove', updateTimeOnMove);
    });
  }

  // Extracting out the mp3 from a gdrive link, using this hack from stackoverflow
  // https://stackoverflow.com/questions/32392072/how-to-play-google-drive-mp3-file-using-html-audio-tag
  // Ideally replace this with a proper audio file link
  const gdrive_file_id = currentEpisode?.url.match("\/d\/(.*)\/view")[1]
  const audio_url = `http://docs.google.com/uc?export=open&id=${gdrive_file_id}`

  return (
    <Container className="player" onPointerDown={(e) => handleTimeDrag(e)}>
      <audio id="audio">
        <source src={audio_url} />
        Your browser does not support the <code>audio</code> element.
      </audio>
      <AudioPlayerBar className="bar__progress" curPercentage={curPercentage} />
      <Spacer />
      <Row style={{ alignItems: 'center' }}>
        <Spacer width={2} />
        <Text style={{ width: '16rem' }}>{formatDuration(currentTime)}</Text>
        <Spacer />
        <Text style={{ textAlign: 'center' }}>
          {currentEpisode ? `You’re listening to: ${currentEpisode.title}` : 'Nothing playing'}
        </Text>
        <Spacer />
        <Text style={{ width: '16rem', textAlign: 'right' }}>{formatDuration(duration)}</Text>
        <Spacer width={2} />
      </Row>
      <Spacer />
    </Container>
  );
};

const Container = styled(Column)`
  position: fixed;
  bottom: 0;
  left: 0;
  right: 0;
  background: white;
  height: 10rem;
`;

const AudioPlayerBar = styled.div<{ curPercentage: number }>`
  width: 100%;
  height: 1.8rem;
  display: flex;
  align-items: center;
  cursor: pointer;
  background: linear-gradient(to right, #44b7ca ${({ curPercentage }) => curPercentage}%, #2f2f2f 0);
`;

const Text = styled.span`
  font-size: 1.8rem;
  color: black;
`;
