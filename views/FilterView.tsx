import React, { useState } from 'react';
import { ChipText, Row, Spacer } from '../common/Common';
import { FilterItem } from './FilterItem';

export const FilterView: React.FC = () => {
  const [selectedFilterOption, setSelectedFilterOption] = useState<string | null>(null);
  const filterOptions = ['date', 'host', 'show name'];
  return (
    <Row style={{ alignItems: 'center', flexWrap: 'wrap' }}>
      <ChipText color="#F8F2F2">Filter</ChipText>
      <Spacer width={2.4} />
      {filterOptions.map((option) => (
        <FilterItem
          key={option}
          title={option}
          isSelected={selectedFilterOption == option}
          onSelect={() => setSelectedFilterOption(selectedFilterOption == option ? null : option)}
        />
      ))}
    </Row>
  );
};
