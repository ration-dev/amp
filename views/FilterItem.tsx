import React from 'react';
import styled from 'styled-components';
import Icons from '../styles/icons';
import { ChipContainer, ChipText, Spacer } from '../common/Common';

type Props = {
  title: string;
  isSelected: boolean;
  onSelect: () => void;
};

export const FilterItem: React.FC<Props> = ({ title, onSelect }) => {
  return (
    <ChipContainer onClick={onSelect} borderColor="#F8F2F2">
      <ChipText color="#F8F2F2">{title}</ChipText>
      <Spacer width={1.2} />
      <FilterIcon src={Icons.ArrowDown} />
    </ChipContainer>
  );
};

const FilterIcon = styled.img`
  width: 1.4rem;
  height: 1.4rem;
`;
