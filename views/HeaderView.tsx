import React from 'react';
import styled from 'styled-components';
import Icons from '../styles/icons';
import { Row, Spacer } from '../common/Common';
import { useWindowSize } from '../hooks/useWindowSize';

export const HeaderView: React.FC = () => {
  const windowSize = useWindowSize();

  if (windowSize.width > 600) {
    return (
      <Container>
        <LogoWrapper>
          <Logo src={Icons.Logo} />
        </LogoWrapper>
        <Spacer />
        <MenuText>MENU</MenuText>
        <Spacer width={2.8} />
        <MenuIcon src={Icons.Menu} />
        <Spacer width={3.2} />
      </Container>
    );
  }

  return (
    <Container>
      <Spacer width={3.2} />
      <Logo src={Icons.Logo} />
      <Spacer />
      <MenuText>MENU</MenuText>
      <Spacer width={2.8} />
      <MenuIcon src={Icons.Menu} />
      <Spacer width={3.2} />
    </Container>
  );
};

const Container = styled(Row)`
  position: fixed;
  top: 0;
  width: 100%;
  height: 12.2rem;
  align-items: center;
  max-width: 156rem;
  background-color: #000000;
`;

const MenuText = styled.span`
  font-size: 4.2rem;
  line-height: 4.2rem;
  color: white;
`;

const MenuIcon = styled.img`
  width: 5rem;
  height: 4.2rem;
`;

const LogoWrapper = styled(Row)`
  position: absolute;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
  align-items: center;
  justify-content: center;
`;

const Logo = styled.img`
  width: 12.7rem;
  height: 4.3rem;
`;
