export type Episode = {
  id: string;
  title: string;
  description: string;
  createdAt: string;
  url: string;
  mediaType: string;
};
